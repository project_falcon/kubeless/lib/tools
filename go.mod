module gitlab.com/project_falcon/kubeless/lib/tools

go 1.14

require (
	github.com/beevik/ntp v0.3.0
	github.com/logrusorgru/aurora/v3 v3.0.0
	github.com/stretchr/testify v1.7.0 // indirect
	gitlab.com/project_falcon/kubeless/lib/payload v1.52.1
	golang.org/x/net v0.0.0-20210220033124-5f55cee0dc0d // indirect
	gopkg.in/yaml.v2 v2.4.0
)
