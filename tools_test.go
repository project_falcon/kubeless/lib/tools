package tools

import (
	"gitlab.com/project_falcon/kubeless/lib/payload"
	"strings"
	"testing"
	"time"
)

func TestRandomString(t *testing.T) {

	length := 20
	randomID := RandomString(length)

	if len(randomID) != length {
		t.Errorf("RandomString returned different size of string, I want %v I got %v", length, len(randomID))
	}

	if strings.Compare(randomID, strings.ToLower(randomID)) != 0 {
		t.Errorf("RandomString strings has UPPERCASES. got %v", randomID)

	}
}

func TestConvertStringRFC3339ToTime(t *testing.T) {

	currentTime := time.Now()
	stringTime := currentTime.Format(time.RFC3339)

	apiEvent := payload.APIData{}

	timeObject, err := ConvertStringRFC3339ToTime(apiEvent, stringTime)

	if err != nil {
		t.Errorf("Cannot parse time correctly got %v", err.Error())
	}

	if timeObject.IsZero() {
		t.Error("Time is zero after parsing")
	}

	if stringTime != timeObject.Format(time.RFC3339) {
		t.Error("Time is not Equal")
	}

}

func TestGetCurrentTimeRFC3339(t *testing.T) {
	stringTime := GetCurrentTimeRFC3339()

	timeObject, err := time.Parse(time.RFC3339, stringTime)

	if err != nil {
		t.Errorf("Cannot parse time correctly got %v", err.Error())
	}

	if timeObject.Format(time.RFC3339) != stringTime {
		t.Error("Time is not Equal")
	}
}

func TestConverRFC3339TimeToString(t *testing.T) {

	currentTime := time.Now()
	stringTime := ConverRFC3339TimeToString(currentTime)

	if stringTime != currentTime.Format(time.RFC3339) {
		t.Error("Time is not Equal")
	}
}
