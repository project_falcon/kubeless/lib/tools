package tools

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"strings"
	"time"

	"github.com/beevik/ntp"
	"github.com/logrusorgru/aurora/v3"
	"gitlab.com/project_falcon/kubeless/lib/payload"
	"gopkg.in/yaml.v2"
)

const (
	//OkColor ok Color
	OkColor = 1
	//InfoColor info color
	InfoColor = 2
	//WarnColor warn color
	WarnColor = 3
	//ErrColor error color
	ErrColor = 4
	//NotifyColor notify color
	NotifyColor = 5

	service       = "tools"
	timeoutSecond = 60
	//lenght of IDString
	IDTaskLenght = 15 //<=16

	EnableLogs  = true
	DisableLogs = false
)

func doHTTPRequest(ctx context.Context, host string, apiEvent *payload.APIData, json []byte) ([]byte, error) {

	// var err error
	// var reply []byte

	httpRequest, err := http.NewRequestWithContext(ctx, "GET", host, bytes.NewBuffer(json))

	if err != nil {
		HandlerMessage(apiEvent, service, fmt.Sprintf("http: cannot prepare newrequest. err '%v'", err.Error()), 500, ErrColor)
		return nil, err
	}

	httpRequest.Header.Set("Content-type", "application/json")

	//because we use context
	// timeout := time.Duration(timeoutSecond * time.Second)
	// client := http.Client{Timeout: timeout}

	client := http.Client{}

	respond, err := client.Do(httpRequest)
	if err != nil {
		HandlerMessage(apiEvent, service, fmt.Sprintf("http: cannot connect to host '%v'. err: '%v'", host, err.Error()), 500, ErrColor)
		return nil, err
	}

	defer respond.Body.Close()

	reply, err := ioutil.ReadAll(respond.Body)

	if err != nil {
		HandlerMessage(apiEvent, service, fmt.Sprintf("http: cannot read answer. err: '%v'", err.Error()), 500, ErrColor)
		return reply, err
	}

	if respond.StatusCode != 200 {
		message := fmt.Sprintf("host '%v' responded with code '%v'.", host, respond.StatusCode)
		HandlerMessage(apiEvent, service, message, 500, ErrColor)

		return reply, errors.New(message)
	}

	return reply, err
}

func GetCurrentTimeFromInternet(apiEvent *payload.APIData, host string) (time.Time){

	// options := ntp.QueryOptions{ Timeout: 30*time.Second, TTL: 5 }

	ntpTime, err := ntp.Time(host)
	if err != nil {
		HandlerMessage(apiEvent, service, fmt.Sprintf("there was problem to get time from '%v', err: '%v'", host, err.Error()), 500, ErrColor)
	}

	// fmt.Printf("Network time: %v\n", ntpTime)
	// timeFormatted := time.Now().UTC().Format(time.UnixDate)

	// fmt.Printf("Unix Date System time: %v\n", timeFormatted)

	return ntpTime

}

//KubelessConnect for connecting to API of kubeless services
func kubelessConnectLogs(ctx context.Context, apiEvent *payload.APIData, action string, host string, enableLogs bool) (*payload.APIData, error) {
	var resData payload.APIData

	//pointers are copied so there is not two different object ! FIX needed ??
	// api := *apiEvent
	api, err := DeepCopyAPIEvent(*apiEvent)
	if err != nil {
		return &api, err
	}
	api.Action = action

	json, _ := json.Marshal(api)
	if enableLogs {
		HandlerMessage(apiEvent, service, fmt.Sprintf("Connecting to host: '%v'. Perform action: '%v'", host, action), 100, NotifyColor)
	}
	res, err := doHTTPRequest(ctx, host, apiEvent, json)

	// api.Msg = "dupa"

	if err != nil {
		HandlerMessage(apiEvent, service, fmt.Sprintf("Err. '%v'", err.Error()), 503, ErrColor)
		// api.Msg = "dupa"
		return &resData, err
	}

	apiRes, err := payload.DecodeAPIJSON(res)

	if err != nil {
		HandlerMessage(apiEvent, service, fmt.Sprintf("Cannot decode API JSON. Err: '%v'", err.Error()), 503, ErrColor)
		return &resData, err
	}

	resData = apiRes

	if apiRes.Code == 404 {
		HandlerMessage(apiEvent, service, apiRes.Msg, apiRes.Code, WarnColor)
		return &resData, errors.New(apiRes.Msg)
	}

	if apiRes.Code >= 400 {
		HandlerMessage(apiEvent, service, apiRes.Msg, apiRes.Code, ErrColor)
		return &resData, errors.New(apiRes.Msg)
	}

	return &resData, err
}

//KubelessConnect for connecting to API of kubeless services
func KubelessConnect(ctx context.Context, apiEvent *payload.APIData, action string, host string) (*payload.APIData, error) {
	respond, err := kubelessConnectLogs(ctx, apiEvent, action, host, EnableLogs)

	return respond, err
}

//KubelessConnect for connecting to API of kubeless services
func KubelessConnectQuiet(ctx context.Context, apiEvent *payload.APIData, action string, host string) (*payload.APIData, error) {
	respond, err := kubelessConnectLogs(ctx, apiEvent, action, host, DisableLogs)

	return respond, err
}

//HandlerMessage for disaplay messages
func HandlerMessage(apiEvent *payload.APIData, service string, msg string, code int, color int) {

	switch color {
	case OkColor:
		status := "OK"
		apiEvent.Msg = fmt.Sprintf("%v: %v: %v", service, status, msg)
		log.Println("ID:", aurora.Yellow(apiEvent.IDtask), aurora.Green(apiEvent.Msg))

	case InfoColor:
		status := "INFO"
		apiEvent.Msg = fmt.Sprintf("%v: %v: %v", service, status, msg)
		log.Println("ID:", aurora.Yellow(apiEvent.IDtask), aurora.BrightYellow(apiEvent.Msg))

	case WarnColor:
		status := "WARN"
		apiEvent.Msg = fmt.Sprintf("%v: %v: %v", service, status, msg)
		log.Println("ID:", aurora.Yellow(apiEvent.IDtask), aurora.Yellow(apiEvent.Msg))

	case ErrColor:
		status := "ERROR"
		apiEvent.Msg = fmt.Sprintf("%v: %v: %v", service, status, msg)
		log.Println("ID:", aurora.Yellow(apiEvent.IDtask), aurora.Red(apiEvent.Msg))

	case NotifyColor:
		status := "NOTIFY"
		apiEvent.Msg = fmt.Sprintf("%v: %v: %v", service, status, msg)
		log.Println("ID:", aurora.Yellow(apiEvent.IDtask), aurora.Blue(apiEvent.Msg))
	}

	apiEvent.Code = code
}

//Respond respond to API client after when JOB is done
func Respond(apiEvent payload.APIData, service string) string {

	resJSON, err := json.Marshal(apiEvent)
	if err != nil {
		HandlerMessage(&apiEvent, service, fmt.Sprintf("Respond cannot Marshal JSON to String. err: '%v'", err.Error()), 417, ErrColor)
	}
	message := fmt.Sprintf("%v: NOTIFY: Send following payload: %s", service, strings.ReplaceAll(string(resJSON), "\\", ""))
	log.Println("ID:", aurora.Yellow(apiEvent.IDtask), aurora.Blue(message))

	return string(resJSON)
}

// //CopyError copy Error Code and Error MSG
// func CopyError(apiEvent *payload.APIData, apiRespond payload.APIData) {
// 	apiEvent.Code = apiRespond.Code
// 	apiEvent.Msg = apiRespond.Msg
// }

//RandomString for generating random string
func RandomString(length int) string {
	const charset = "abcdefghijklmnopqrstuvwxyz" + "0123456789"
	var seededRand *rand.Rand = rand.New(rand.NewSource(time.Now().UnixNano()))

	b := make([]byte, length)
	for i := range b {
		b[i] = charset[seededRand.Intn(len(charset))]
	}

	return string(b)
}

//GetCurrentTimeRFC3339 get current time in string format
func GetCurrentTimeRFC3339() string {
	//that is object
	currentTime := time.Now()

	//this time should be stored as String
	return currentTime.Format(time.RFC3339)
}

//ConvertStringRFC3339ToTime conver string time to Time object
func ConvertStringRFC3339ToTime(apiEvent payload.APIData, stringTmime string) (time.Time, error) {
	timeObject, err := time.Parse(time.RFC3339, stringTmime)

	if err != nil {
		HandlerMessage(&apiEvent, service, fmt.Sprintf("Cannot parset string Time '%v' to Time.RFC3339 object. Err: '%v'", stringTmime, err.Error()), 301, ErrColor)
	}

	return timeObject, err
}

//ConverRFC3339TimeToString convert time object to string
func ConverRFC3339TimeToString(t time.Time) string {

	return t.Format(time.RFC3339)
}

//ParseYamlToMap conver YAML to MAP
func ParseYamlToMap(arrayByte []byte) (map[interface{}]interface{}, error) {
	m := make(map[interface{}]interface{})

	err := yaml.Unmarshal(arrayByte, &m)

	return m, err
}

//DeepCopyAPIEvent make deep copy of APIEvent object
func DeepCopyAPIEvent(apiEvent payload.APIData) (payload.APIData, error) {

	copyByte, err := json.Marshal(apiEvent)
	if err != nil {
		HandlerMessage(&apiEvent, service, fmt.Sprintf("There is problem to marshal ApiEvent Object. Err: '%v'", err.Error()), 500, ErrColor)
		return apiEvent, err
	}

	var copyAPIEvent payload.APIData

	err = json.Unmarshal(copyByte, &copyAPIEvent)
	if err != nil {
		HandlerMessage(&apiEvent, service, fmt.Sprintf("There is problem to Unmarshal back ApiEvent Object. Err: '%v'", err.Error()), 500, ErrColor)
	}

	return copyAPIEvent, err
}
